DROP DATABASE IF EXISTS MobilificioIo;
CREATE DATABASE MobilificioIo;
USE MobilificioIo;

CREATE TABLE Categoria(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL UNIQUE,
    descrizione TEXT DEFAULT ("Non definita"),
    codice VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE Oggetto(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(100) NOT NULL UNIQUE,
    descrizione TEXT DEFAULT NULL,
    codice VARCHAR(100) NOT NULL UNIQUE,
    prezzo FLOAT NOT NULL
);

CREATE TABLE OggettoCategoria(
	idOggetto INTEGER NOT NULL,
    idCategoria INTEGER NOT NULL,
    PRIMARY KEY (idOggetto, idCategoria),
	FOREIGN KEY (idOggetto) REFERENCES Oggetto(id),
    FOREIGN KEY (idCategoria) REFERENCES Categoria(id)
);

INSERT INTO Oggetto (nome, descrizione, codice, prezzo) VALUES
	("Sedia ky", "Sedia in legno con schienale", "SKY123", 15.15),
    ("Sedia ry", "Sgabello 50 cm", "SGA123", 25.15),
    ("TavoloTrs", "Tavolo in vetro", "TA123", 50);

INSERT INTO Categoria (nome, codice) VALUES
	("Sedie", "SED"),
    ("Tavoli", "TAV"),
    ("Camera da pranzo", "PRA");

INSERT INTO OggettoCategoria (idOggetto, idCategoria) VALUES
	(1, 1),
    (1, 3),
    (2, 1),
    (3, 2),
    (3, 3);


INSERT INTO Categoria (nome,descrizione,codice) VALUE ("lampada","lampada in vetro","LAMACC");
SELECT id, nome, descrizione, codice FROM Categoria;

-- Ricerca categorie di un oggetto dato il codice
SELECT Categoria.nome FROM Oggetto
	JOIN OggettoCategoria ON Oggetto.id = OggettoCategoria.idOggetto
    JOIN Categoria ON OggettoCategoria.idCategoria = Categoria.id
    WHERE Oggetto.codice = "SKY123";