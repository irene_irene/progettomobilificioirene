package com.mobilificio.io.model;

import java.util.ArrayList;

public class Oggetto {

	private Integer id;
	private String nome;
	private String descrizione;
	private String codice;
	private Float prezzo;
	private ArrayList<Categoria> elencoCategoria;
	
	
	public ArrayList<Categoria> getElencoCategoria() {
		return elencoCategoria;
	}
	public void setElencoCategoria(ArrayList<Categoria> elencoCategoria) {
		this.elencoCategoria = elencoCategoria;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public Float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}
	
}
