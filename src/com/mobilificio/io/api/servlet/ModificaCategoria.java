package com.mobilificio.io.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.io.api.services.CategorieDao;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.utility.ResponsoOperazione;

@WebServlet("/modificacategoria")
public class ModificaCategoria extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategorieDao cd = new CategorieDao();
		String varIds =request.getParameter("id");
		
		try {
			int varId = Integer.parseInt(varIds);
			Categoria catTemp = cd.getById(varId);
			
			String codNuovo = request.getParameter("codice").trim();
			String nomeNuovo = request.getParameter("nome").trim();
			
			if(!codNuovo.isEmpty() && !nomeNuovo.isEmpty()) {
				
				String descrizioneNuovo = request.getParameter("descrizione") != null ? request.getParameter("descrizione").trim() : "";
				
				catTemp.setCodice(codNuovo);
				catTemp.setDescrizione(descrizioneNuovo);
				catTemp.setNome(nomeNuovo);
				
				if(cd.update(catTemp)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "Modifica riuscita");
					out.print(new Gson().toJson(res));
				}
			}
			else {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nome e codice obbligatori");
				out.print(new Gson().toJson(res));
			}
				
				
		} catch (SQLException | NullPointerException | NumberFormatException e) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", e.getMessage());
			out.print(new Gson().toJson(res));
		}
	}
}
