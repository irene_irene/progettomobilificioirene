package com.mobilificio.io.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.io.api.services.CategorieDao;
import com.mobilificio.io.api.services.OggettoDao;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.model.Oggetto;
import com.mobilificio.io.utility.ResponsoOperazione;

@WebServlet("/inseriscioggetto")
public class InserisciOggetto extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			String varNome = request.getParameter("nome");
			String varDescrizione = request.getParameter("descrizione") != null ? request.getParameter("descrizione").trim() : "";
			String varCodice = request.getParameter("codice");
			String varPrezzos = request.getParameter("prezzo");
			String varCategories = request.getParameter("categorie");
			
			Float varPrezzo = Float.parseFloat(varPrezzos.trim());
			String[] varCategorie= new Gson().fromJson(varCategories, String[].class);
			
//			System.out.println(varNome);
//			System.out.println(varCategories);
			
			if(!varNome.trim().isEmpty() && !varCodice.trim().isEmpty() && varCategorie.length>0) {
				
				OggettoDao cdOggetto = new OggettoDao();
				
				Oggetto objOggetto = new Oggetto();
				objOggetto.setNome(varNome);
				objOggetto.setDescrizione(varDescrizione);
				objOggetto.setCodice(varCodice);
				objOggetto.setPrezzo(varPrezzo);
				
				cdOggetto.insert(objOggetto);
				
				if(objOggetto.getId() != null) {
					CategorieDao cdCategoria = new CategorieDao();
					
					for(int i=0; i<varCategorie.length;i++) {
						Categoria objCategoria = cdCategoria.getByUniqueCode(varCategorie[i]);
						
						if(!cdOggetto.insertRelazione(objOggetto, objCategoria)) {
							ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Errore di inserimento relazione");
							out.print(new Gson().toJson(res));
						}					
					}
										
					ResponsoOperazione res = new ResponsoOperazione("OK", "Oggetto inserito");
					out.print(new Gson().toJson(res));					
				}
				
			}
			else {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Completa correttamente tutti i campi");
				out.print(new Gson().toJson(res));
		
			}
			
		} catch (NullPointerException| NumberFormatException e) {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Completa correttamente tutti i campi");
			out.print(new Gson().toJson(res));
		}catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
			out.print(new Gson().toJson(res));
		};
		

		
		
	}

}
