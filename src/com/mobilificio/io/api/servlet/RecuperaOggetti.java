package com.mobilificio.io.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.io.api.services.CategorieDao;
import com.mobilificio.io.api.services.OggettoDao;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.model.Oggetto;
import com.mobilificio.io.utility.ResponsoOperazione;

@WebServlet("/recuperaoggetti")
public class RecuperaOggetti extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		OggettoDao cd = new OggettoDao();
		
		try {
			ArrayList<Oggetto> elencoOggetti = cd.getAll();
			
			ResponsoOperazione ris = new ResponsoOperazione("OK", new Gson().toJson(elencoOggetti));
			out.print(new Gson().toJson(ris));
			
		} catch (SQLException e) {
			
			ResponsoOperazione ris = new ResponsoOperazione("ERRORE", e.getMessage());
			out.print(new Gson().toJson(ris));
			
		}
	}

}
