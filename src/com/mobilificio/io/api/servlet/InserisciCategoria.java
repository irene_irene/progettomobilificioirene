package com.mobilificio.io.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.io.api.services.CategorieDao;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.utility.ResponsoOperazione;

@WebServlet("/inseriscicategoria")
public class InserisciCategoria extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String varNome = request.getParameter("nome").trim();
		String varDescrizione = request.getParameter("descrizione") != null ? request.getParameter("descrizione").trim() : "";
		String varCodice = request.getParameter("codice").trim();
		
		Categoria temp = new Categoria();
		temp.setNome(varNome);
		temp.setDescrizione(varDescrizione);
		temp.setCodice(varCodice);
		
		CategorieDao cd = new CategorieDao();
		Gson jsonizzatore = new Gson();
		
		try {
			
			cd.insert(temp);
			ResponsoOperazione res = new ResponsoOperazione("OK", "Categoria inserita");
			out.print(jsonizzatore.toJson(res));
			
		} catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
			out.print(jsonizzatore.toJson(res));
		}	
		
	}

}
