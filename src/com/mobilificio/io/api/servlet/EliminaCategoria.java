package com.mobilificio.io.api.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.io.api.services.CategorieDao;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.utility.ResponsoOperazione;

@WebServlet("/eliminacategoria")
public class EliminaCategoria extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
		out.print(new Gson().toJson(res));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategorieDao cd = new CategorieDao();
		String varCodice = request.getParameter("codselezionato");
		
		try {
			Categoria catTemp = cd.getByUniqueCode(varCodice);
			if(cd.delete(catTemp)) {
				ResponsoOperazione res = new ResponsoOperazione("OK", "Eliminazione riuscita");
				out.print(new Gson().toJson(res));
			}
			
		} catch (SQLException e) {
			ResponsoOperazione res = new ResponsoOperazione("Errore", e.getMessage());
			out.print(new Gson().toJson(res));
		}
		
		
	}

}
