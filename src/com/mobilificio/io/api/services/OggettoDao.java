package com.mobilificio.io.api.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mobilificio.io.connessione.ConnettoreMySQL;
import com.mobilificio.io.model.Categoria;
import com.mobilificio.io.model.Oggetto;

public class OggettoDao implements Dao<Oggetto>{

	@Override
	public Oggetto getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Oggetto getByUniqueCode(String cod) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {
		
		ArrayList<Oggetto> elencoOggetti = new ArrayList<Oggetto>();
		
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		String query = "SELECT id, nome, descrizione, codice, prezzo FROM Oggetto";
		PreparedStatement ps =  conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		
		while(risultato.next()) {
			Oggetto oggTemp = new Oggetto();
			
			oggTemp.setId(risultato.getInt(1));
			oggTemp.setNome(risultato.getString(2));
			oggTemp.setDescrizione(risultato.getString(3));
			oggTemp.setCodice(risultato.getString(4));
			oggTemp.setPrezzo(risultato.getFloat(5));
			
			ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
			elencoCategorie = getAllCategorie(oggTemp.getId());
			oggTemp.setElencoCategoria(elencoCategorie);
			
			elencoOggetti.add(oggTemp);
		}
		
		return elencoOggetti;
	}

	public ArrayList<Categoria> getAllCategorie(Integer id) throws SQLException {

		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
		
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		String query = "SELECT Categoria.id, Categoria.nome, Categoria.descrizione, Categoria.codice  FROM Oggetto"
				+ " JOIN OggettoCategoria ON Oggetto.id = OggettoCategoria.idOggetto"
				+ " JOIN Categoria ON OggettoCategoria.idCategoria = Categoria.id"
				+ " WHERE Oggetto.id = ?";
		
		PreparedStatement ps =  conn.prepareStatement(query);
		ps.setInt(1, id);
		
		ResultSet risultato = ps.executeQuery();	
		
		while(risultato.next()) {
			Categoria catTemp = new Categoria();
			
			catTemp.setId(risultato.getInt(1));
			catTemp.setNome(risultato.getString(2));
			catTemp.setDescrizione(risultato.getString(3));
			catTemp.setCodice(risultato.getString(4));
			
			elencoCategorie.add(catTemp);
		}
		
		return elencoCategorie;
	}
	
	
	@Override
	public void insert(Oggetto t) throws SQLException {
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		
		String query = "INSERT INTO Oggetto (nome,descrizione,codice,prezzo) VALUE (?,?,?,?)";				
		PreparedStatement ps =  conn.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		ps.setString(3, t.getCodice());
		ps.setFloat(4, t.getPrezzo());
		
		ps.executeUpdate();
		
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
		
	}

	public boolean insertRelazione(Oggetto t, Categoria c) throws SQLException {
		boolean risultato = false;
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		
		String query = "INSERT INTO OggettoCategoria (idOggetto, idCategoria) VALUE (?,?)";				
		PreparedStatement ps =  conn.prepareStatement(query);
		ps.setInt(1, t.getId());
		ps.setInt(2, c.getId());

		int rowAffected = ps.executeUpdate();
		if(rowAffected>0)
       		risultato = true;
   		
       	return risultato;
		
	}
	
	@Override
	public boolean delete(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
