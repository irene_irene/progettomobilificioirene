package com.mobilificio.io.api.services;

import java.util.ArrayList;

import com.mobilificio.io.connessione.ConnettoreMySQL;
import com.mobilificio.io.model.Categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CategorieDao implements Dao<Categoria>{

	@Override
	public Categoria getById(int id) throws SQLException {
  		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
        
       	String query = "SELECT id, nome, descrizione, codice FROM Categoria WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Categoria temp = new Categoria();
   		temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
       	
       	return temp;
	}
	
	@Override
	public Categoria getByUniqueCode(String cod) throws SQLException {
  		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
        
       	String query = "SELECT id, nome, descrizione, codice FROM Categoria WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, cod);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

   		Categoria temp = new Categoria();
   		temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
       	
       	return temp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
		
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		String query = "SELECT id, nome, descrizione, codice FROM Categoria";
		PreparedStatement ps =  conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		
		while(risultato.next()) {
			Categoria catTemp = new Categoria();
			
			catTemp.setId(risultato.getInt(1));
			catTemp.setNome(risultato.getString(2));
			catTemp.setDescrizione(risultato.getString(3));
			catTemp.setCodice(risultato.getString(4));
			
			elencoCategorie.add(catTemp);
		}
		
		return elencoCategorie;
		
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		
		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
		
		String query = "INSERT INTO Categoria (nome,descrizione,codice) VALUE (?,?,?)";				
		PreparedStatement ps =  conn.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		ps.setString(3, t.getCodice());
		
		ps.executeUpdate();
		
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
       	
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
   		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
   		
   		String query = "DELETE FROM Categoria WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}
	

	@Override
	public boolean update(Categoria t) throws SQLException {
   		Connection conn = ConnettoreMySQL.getIstance().getConnessione();
   		
   		String query = "UPDATE Categoria SET"
   				+ "	nome = ?,"
   				+ " descrizione = ?,"
   				+ " codice = ? "
   				+ " WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getDescrizione());
       	ps.setString(3, t.getCodice());
       	ps.setInt(4, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}


}
