package com.mobilificio.io.connessione;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreMySQL {
	
	private Connection conn;
	private static ConnettoreMySQL ogg_connessione;
	
	public static ConnettoreMySQL getIstance() {
		if(ogg_connessione == null)
			ogg_connessione = new ConnettoreMySQL();
		
		return ogg_connessione;	
	}
	
	public Connection getConnessione() throws SQLException {
		if (conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");
			dataSource.setPort(3306);
			dataSource.setUser("root");
			dataSource.setPassword("Toor");
			dataSource.setUseSSL(false);
			dataSource.setDatabaseName("MobilificioIo");
			
			conn = dataSource.getConnection();			
		}
		return conn;
		
	}
}