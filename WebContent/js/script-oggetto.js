var modificaAbilitata = false;
var cancellaAbilitata = false;

function stampaRighe(arr_oggetti){
	
	righe = "";
	for (let i=0; i<arr_oggetti.length;i++){
		oggTemp = arr_oggetti[i];
		
		let arrCategorie = oggTemp.elencoCategoria;
		let strCategorie = "";
		for (let k=0; k<arrCategorie.length; k++){
			strCategorie += arrCategorie[k].nome;
			if(k!= (arrCategorie.length-1))
				strCategorie += ", ";
		}
		//console.log(strCategorie);
		
		let rigaModifica = "";
		if(!modificaAbilitata == true)
			rigaModifica = 'hidden="hidden"';
		
		let rigaCancella = "";
		if(!cancellaAbilitata == true)
			rigaCancella = 'hidden="hidden"';
		
		let rigaTemp = '<tr data-identificativo="'+ oggTemp.codice+ '">';
			rigaTemp += '<td>' + oggTemp.nome + '</td>';
			rigaTemp += '<td>' + oggTemp.descrizione + '</td>';
			rigaTemp += '<td>' + oggTemp.codice + '</td>';
			rigaTemp += '<td>' + oggTemp.prezzo + '</td>';
			rigaTemp += '<td>' + strCategorie + '</td>';			
			rigaTemp += '<td><button type="button" class="btn btn-danger btn-sm btn-cancella" ' + rigaCancella + 'onclick="eliminaCategoria(this)"><i class="fas fa-trash"></i></button>';
			rigaTemp += ' <button type="button" class="btn btn-warning btn-sm btn-modifica"' + rigaModifica + 'onclick="modificaCategoria(this)"><i class="fas fa-pen-fancy"></i></button></td>';
			rigaTemp += '</tr>';
		
		righe += rigaTemp;
	}
	
	$("#stampaOggetti").html(righe); 
	
}


function richiestaAggiornaTabella(){
	$.ajax(
		{
			url: "http://localhost:8080/CorsoNTTMobilificioIo/recuperaoggetti",
			method: "POST",
			success: function(risultato){
				switch(risultato.risultato){
					case "OK":
						let dettaglioJson = JSON.parse(risultato.dettaglio);
						stampaRighe(dettaglioJson);
						break;
					case "ERRORE":
						alert("ERRORE:(\n" + risultato.dettaglio);
						break;
					}
			},
			error: function(risultato){
				console.log(risultato);
			},
		}		
		
	)
	
}

function stampaCheckBox(arr_categorie){
	let str = "";
	for(let i=0; i<arr_categorie.length; i++){
		str += '<div class="form-check form-check-inline">';		
		str += '<input class="form-check-input form-cliccato" type="checkbox" id="inlineCheckbox1" value='+ arr_categorie[i].codice+'>';
		str += '<label class="form-check-label" for="inlineCheckbox1">'+ arr_categorie[i].nome+'</label>';
		str += '</div>'
	}
	
	$("#selezionaCategorie").html(str); 
	
}

function stampaCheckCategorie(){
	$.ajax(
		{
			url: "http://localhost:8080/CorsoNTTMobilificioIo/recuperacategorie",
			method: "POST",
			success: function(risultato){
				switch(risultato.risultato){
					case "OK":
						let dettaglioJson = JSON.parse(risultato.dettaglio);
						//console.log(dettaglioJson);
						stampaCheckBox(dettaglioJson);
						break;
					case "ERRORE":
						alert("ERRORE:(\n" + risultato.dettaglio);
						break;
					}
			},
			error: function(risultato){
				console.log(risultato);
			},
		}		
		
	)
	
}


$(document).ready(
	function(){
		richiestaAggiornaTabella();
 		
		window.setInterval(
			function(){
				richiestaAggiornaTabella();
				console.log("Sto aggiornando la tabella categorie");			
			}
		, 4000);
		
		$("#inserimentoCategoria").click(
			function(){
				stampaCheckCategorie();
				$("#modaleInserimento").modal("show");
			}
			
		);
		
		$("#inserisciModale").click(
			function(){
				
				let varNome = $("#nome").val();
				let varDescrizione = $("#descrizione").val();
				let varCodice = $("#codice").val();
				let varPrezzo = $("#prezzo").val();
				let categorieScelte = [];
				
				let categorie = $(".form-cliccato");	
				for (let i=0;i<categorie.length;i++){
					if (categorie[i].checked){
						let codCategoria = $(categorie[i]).val();
						//console.log(codCategoria);
						categorieScelte.push(codCategoria);
					}
						
				}
				
				console.log(varNome);
				console.log(categorieScelte);
				 $.ajax(
					{
						url: "http://localhost:8080/CorsoNTTMobilificioIo/inseriscioggetto",
						method: "POST",
						data:{
							nome: varNome,
							descrizione: varDescrizione,
							codice: varCodice,
							prezzo: varPrezzo,
							categorie: JSON.stringify(categorieScelte)
						},
						success: function(risultato){			
							switch(risultato.risultato){
								case "OK":
									alert(risultato.dettaglio);
									break;
								case "ERRORE":
									alert("ERRORE:(\n" + risultato.dettaglio);
									break;								
								}
								
							$("#modaleInserimento").modal("toggle");
							modificaAbilitata = false;
							richiestaAggiornaTabella();
						},
						error: function(risultato){
							console.log(risultato);
						},
					}		
					
				)				
				
			}
			
		);
		
		$("#modificaCategoria").click(
			function(){
				modificaAbilitata = true;
				$(".btn-cancella").prop("hidden","hidden");
				cancellaAbilitata = false;
				$(".btn-modifica").prop("hidden",false);
			}
			
		);		
		
		$("#cancellaCategoria").click(
			function(){
				cancellaAbilitata = true;
				$(".btn-modifica").prop("hidden","hidden");
				modificaAbilitata = false;
				$(".btn-cancella").prop("hidden",false);
			}
			
		);
	}

);