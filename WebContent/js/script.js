var modificaAbilitata = false;
var cancellaAbilitata = false;

function stampaRighe(arr_categorie){
	
	righe = "";
	for (let i=0; i<arr_categorie.length;i++){
		catTemp = arr_categorie[i];
		
		let rigaTemp = '<tr data-identificativo="'+ catTemp.codice+ '">';
			rigaTemp += '<td>' + catTemp.nome + '</td>';
			rigaTemp += '<td>' + catTemp.descrizione + '</td>';
			rigaTemp += '<td>' + catTemp.codice + '</td>';
			
			let rigaModifica = "";
			if(!modificaAbilitata == true)
				rigaModifica = 'hidden="hidden"';
			
			let rigaCancella = "";
			if(!cancellaAbilitata == true)
				rigaCancella = 'hidden="hidden"';
				
			rigaTemp += '<td><button type="button" class="btn btn-danger btn-sm btn-cancella" onclick="eliminaCategoria(this)"' + rigaCancella + '><i class="fas fa-trash"></i></button>';
			rigaTemp += ' <button type="button" class="btn btn-warning btn-sm btn-modifica" onclick="modificaCategoria(this)"' + rigaModifica + '><i class="fas fa-pen-fancy"></i></button></td>';
			//rigaTemp += ' <button type="button" class="btn btn-warning btn-sm btn-modifica" onclick="modificaCategoria(this)" hidden="hidden"><i class="fas fa-pen-fancy"></i></button></td>';
			rigaTemp += '</tr>';
		
		righe += rigaTemp;
	}
	
	$("#stampaCategorie").html(righe); 
	
}

function modificaCategoria(objButton){
	
	let bottone = $(objButton);
	let codDaModificare = bottone.parent().parent().data("identificativo");
	//console.log(codDaModificare);
	
	$.ajax(
		{
			url: "http://localhost:8080/CorsoNTTMobilificioIo/recuperacategorie",
			method: "POST",
			
			data:{
				codselezionato: codDaModificare
			},
			success: function(risultato){
				//console.log(risultato);
				let dettaglioJson = JSON.parse(risultato.dettaglio);
				$("#nomeModifica").val(dettaglioJson.nome);
				$("#descrizioneModifica").val(dettaglioJson.descrizione);
				$("#codiceModifica").val(dettaglioJson.codice);	
				
				$("#modaleModifica").data("identificatore", dettaglioJson.id);
				$("#modaleModifica").modal("show");
				
			},
			error: function(risultato){
				console.log(risultato);
			},
		}		
		
	)
		
}

function eliminaCategoria(objButton){
	let bottone = $(objButton);
	let codSelezionato = bottone.parent().parent().data("identificativo");
	console.log(codSelezionato);
	
	$.ajax(
		{
			url: "http://localhost:8080/CorsoNTTMobilificioIo/eliminacategoria",
			method: "POST",
			data: {
				codselezionato: codSelezionato
			},
			success: function(risultato){
				switch(risultato.risultato){
				case "OK":
					cancellaAbilitata = false;
					richiestaAggiornaTabella();
					alert(risultato.dettaglio);
					break;
				case "ERRORE":
					alert("ERRORE DI ELIMINAZIONE :(\n" + risultato.dettaglio);
					break;
				}
				
			},
			error: function(errore){
				console.log(errore);
			}
		}
	);
}


function richiestaAggiornaTabella(){
	$.ajax(
		{
			url: "http://localhost:8080/CorsoNTTMobilificioIo/recuperacategorie",
			method: "POST",
			success: function(risultato){
				switch(risultato.risultato){
					case "OK":
						let dettaglioJson = JSON.parse(risultato.dettaglio);
						stampaRighe(dettaglioJson);
						break;
					case "ERRORE":
						alert("ERRORE:(\n" + risultato.dettaglio);
						break;
					}
			},
			error: function(risultato){
				console.log(risultato);
			},
		}		
		
	)
	
}


$(document).ready(
	function(){
		richiestaAggiornaTabella();
 
		window.setInterval(
			function(){
				richiestaAggiornaTabella();
				console.log("Sto aggiornando la tabella categorie");			
			}
		, 4000);
		
		
		$("#inserimentoCategoria").click(
			function(){
				$("#modaleInserimento").modal("show");
			}
			
		);
		
		
		$("#modificaCategoria").click(
			function(){
				modificaAbilitata = true;
				$(".btn-cancella").prop("hidden","hidden");
				cancellaAbilitata = false;
				$(".btn-modifica").prop("hidden",false);
			}
			
		);
		
		
		$("#cancellaCategoria").click(
			function(){
				cancellaAbilitata = true;
				$(".btn-modifica").prop("hidden","hidden");
				modificaAbilitata = false;
				$(".btn-cancella").prop("hidden",false);
			}
			
		);

		$("#inserisciModale").click(
			function(){
				let varNome = $("#nome").val();
				let varDescrizione = $("#descrizione").val();
				let varCodice = $("#codice").val();		 
				
				 $.ajax(
					{
						url: "http://localhost:8080/CorsoNTTMobilificioIo/inseriscicategoria",
						method: "POST",
						data:{
							nome: varNome,
							descrizione: varDescrizione,
							codice: varCodice
						},
						success: function(risultato){			
							switch(risultato.risultato){
								case "OK":
									alert(risultato.dettaglio);
									break;
								case "ERRORE":
									alert("ERRORE:(\n" + risultato.dettaglio);
									break;								
								}
								
							$("#modaleInserimento").modal("toggle");
							modificaAbilitata = false;
							richiestaAggiornaTabella();
						},
						error: function(risultato){
							console.log(risultato);
						},
					}		
					
				)
			}
		);		
		
		$("#modificaModale").click(
			function(){
				let varNome = $("#nomeModifica").val();
				let varDescrizione = $("#descrizioneModifica").val();
				let varCodice = $("#codiceModifica").val();	
				let idDaModificare = $("#modaleModifica").data("identificatore");
				 
				
				 $.ajax(
					{
						url: "http://localhost:8080/CorsoNTTMobilificioIo/modificacategoria",
						method: "POST",
						data:{
							id:idDaModificare,
							nome: varNome,
							descrizione: varDescrizione,
							codice: varCodice
						},
						success: function(risultato){
							console.log(risultato);
							console.log(risultato.risultato);						
							switch(risultato.risultato){
								case "OK":
									alert("Categoria modificata");
									break;
								case "ERRORE":
									alert("ERRORE:(\n" + risultato.dettaglio);
									break;								
								}
								
							$("#modaleModifica").modal("toggle");
							modificaAbilitata = false;
							richiestaAggiornaTabella();
						},
						error: function(risultato){
							console.log(risultato);
						},
					}		
					
				)
			}
		);			
	}
	
);